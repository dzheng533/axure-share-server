#!/bin/bash

set -e
ls /app/server/dockers
nginx -t
nginx
node -v
cd /app/server/
yarn run start
#exec "$@";
