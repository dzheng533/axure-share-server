
Set-Location ../web
yarn build
Set-Location ../

Remove-Item 'public' -Recurse

New-Item -Path 'public' -ItemType Directory

Copy-Item -Path 'web\dist\*.*' -Destination 'public\' -Force

yarn package
