#!/bin/bash

cd ../web
yarn build
cd ../

if [ -d "./public" ]; then
   rm -rf public
fi
mkdir public
cp web/dist/* public 

if [ -d "./out" ]; then
   rm -rf out
fi
yarn package
