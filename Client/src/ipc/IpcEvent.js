module.exports = {
    "COMMON_FULL_SCREEN":"full_screen",
    "COMMON_EXIT_FULL_SCREEN":"exit_full_screen",
    "COMMON_OPENURL":"open_url",

    "LIST_PROJECTS":"list_projects",
    "ADD_PROJECT":"add_project",
    "VIEW_PROJECT":"view_project",
    "ADD_VERSION":"add_version",
    "ADD_VERSION_PROCESS":"add_version_process",
    "OPEN_FOLDER":"open_folder",
    'LOAD_APPCONFIG':'load-appconfig', //加载App配置
    'SAVE_APPCONFIG':'save-appconfig', //保存App配置
}