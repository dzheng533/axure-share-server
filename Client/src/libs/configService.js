const storage = require('./localStorage');
const SERVERS = 'SERVERS';
const APPS = 'APPS';
class ConfigService{
    constructor(){
        this.init();
    }
    init(){
        storage.initStorage();
        let servers = storage.getItem(SERVERS);
        if(servers === null || servers === ''){
            storage.setItem(SERVERS,{});
            storage.setItem(APPS,{uploadServer:'192.168.80.101:3000'});
        }
        this.servers = storage.getItem(SERVERS);
        this.appconfig = storage.getItem(APPS);
    }
    addServer(name,item){
        this.servers[name] = item;
        storage.setItem(SERVERS,this.servers);
    }
    loadServerList(){
        let serverList = [];
        Object.keys(this.servers).forEach((item)=>{
            serverList.push(this.servers[item]);
        })
        return serverList;
    }
    getServerConfig(name){
        if(this.servers.hasOwnProperty(name)){
            return this.servers[name];
        }else{
            return null;
        }
    }
    removeAll(){
        this.servers = {};
        storage.setItem(SERVERS,this.servers);
        storage.removeAll();
    }
    removeServerConfig(name){
        if(this.servers.hasOwnProperty(name)){
            this.servers[name] = null;
            storage.setItem(SERVERS,this.servers);
            return true;
        }else{
            return false;
        }
    }
    saveAppConfig(config){
        if(config){
            this.appconfig = Object.assign({},config);
            storage.setItem(APPS,this.appconfig);
        }
    }
    loadAppConfig(){
        if(this.appconfig){
            return this.appconfig;
        }else{
            return {};
        }
    }
}
module.exports = ConfigService;