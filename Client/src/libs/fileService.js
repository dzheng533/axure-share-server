const fs = require('fs');
const AdmZip = require("adm-zip");

/**
 * 压缩目录
 * @param {待压缩的目录} path 
 * @returns 如果成功返回Buffer格式的压缩后数据。 null 表示失败。
 */
const compressFolder = (path,cb) =>{
    return new Promise((resolve, reject) => {
        if(fs.existsSync(path)){
            var zip = new AdmZip();
            zip.addLocalFolder(path);
            let buffer = zip.toBuffer();
            resolve(buffer);
        }else{
            reject("File Not exist");
        }
    });
}

module.exports = {compressFolder};