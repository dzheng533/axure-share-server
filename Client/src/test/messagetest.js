const io = require("socket.io-client");
const EventEmitter = require('events');


 const socket = io("ws://127.0.0.1:9092");

 const onConnectHandler = () => {
    console.log("server connected:", socket.id);
    socket.emit("message",{client:"ddd"},(ack)=>{
        console.log("ack:",ack);
    })
}
 socket.on("connect",  onConnectHandler);
 socket.on("hello",(arg)=>{
    console.log("Hello message:", arg);
 })
