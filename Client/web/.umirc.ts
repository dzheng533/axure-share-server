import { defineConfig } from 'umi';

export default defineConfig({
  title:'原型管理客户端',
  publicPath:'./',
  history:{type:'hash'},
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index' }
  ],
  fastRefresh: {},
});
