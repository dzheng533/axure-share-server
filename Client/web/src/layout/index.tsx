import React from 'react';
import { Link } from 'umi';
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.less';

const {Header,Content} = Layout;

const menus = [{
  key:'0',
  path: '/',
  name:'首页'
},
{
  key:'1',
  path: '/menu',
  name:'菜单迁移'
},
{
  key:'2',
  path: '/report',
  name:'报表管理'
},
{
  key:'3',
  path: '/rule',
  name:'规则引擎'
},
{
  key:'4',
  path: '/indexcfg',
  name:'指标配置'
},
{
  key:'5',
  path: '/env',
  name:'环境配置'
},{
  key:'6',
  path: '/metadata',
  name:'元数据管理'
},
{
  key:'100',
  path: '/test',
  name:'测试'
}]

export default (props) => {

  const {location,children} = props;
  console.log("children:",location,props);
  let selectKey = ['0'];
  const matchMenu = menus.filter(menu =>{return menu.path === location.pathname});
  if(matchMenu && matchMenu.length >0){
    selectKey[0] = matchMenu[0].key;
  }

  if (location.pathname === '/') {
    return (<Layout className="layout">
    <Header>
    <div className="logo" />
    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['0']} selectedKeys={selectKey}>
      {menus.map( menu =>{
        return  <Menu.Item key={menu.key}><Link to={menu.path}>{menu.name}</Link></Menu.Item>
      })}
    </Menu>
  </Header>
  <Content style={{ padding: '0 50px' }}>
    <div className="site-layout-content">
      content
    </div>
  </Content>
  </Layout>);

  }else{
    return  (<Layout className="layout">
    <Header>
    <div className="logo" />
    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['0']} selectedKeys={selectKey}>
      {menus.map( menu =>{
        return  <Menu.Item key={menu.key}><Link to={menu.path}>{menu.name}</Link></Menu.Item>
      })}
    </Menu>
  </Header>
  <Content style={{ padding: '0 50px' }}>
    <div className="site-layout-content">
    {children}
    </div>
  </Content>
  </Layout>);
  }

  
}