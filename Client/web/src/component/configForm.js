import React, { useState, useEffect, useRef } from 'react';
import { Form, Input } from 'antd';

import {ipcClient} from '../../../src/ipc/IpcClent';

export default function configForm({form,closeHandler}) {
    const [appconfig,setAppConfig] = useState(null);
    const submitConfig = (config)=>{
        console.log(config)
        const newConfig = {...appconfig,uploadServer:config.serverName};
        ipcClient.saveConfig(newConfig,(res)=>{
            console.log(res);
            if(res.code===1000){
                closeHandler && closeHandler();
            }
            //configForm.setFieldsValue({serverName:config.uploadServer});
            //setAppConfig(config);
        });
    }
    const [configForm] = Form.useForm(form);
    useEffect(()=>{
        ipcClient.loadConfig(null,(config)=>{
            console.log(config);
            configForm.setFieldsValue({serverName:config.uploadServer});
            setAppConfig(config);
        });

    },[]);

    return (
        <Form form={form} layout="vertical" name="configForm" onFinish={submitConfig}>
          <Form.Item name="serverName" label="服务器地址：" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
    );
  };