import React, { useState, useEffect, useRef } from 'react';
import { Form, Input, InputNumber, Modal, Button, Avatar, Typography } from 'antd';

const {TextArea} = Input

export default function ProjectForm({ form }) {
    return (
        <Form form={form} layout="vertical" name="userForm">
          <Form.Item name="projectName" label="项目名称" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="age" label="User Age" rules={[{ required: true }]}>
          <TextArea allowClear='true' showCount='true' placeholder="变更记录" />
          </Form.Item>
        </Form>
    );
  };